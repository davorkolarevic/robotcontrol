import math
from contracts.direction import Direction
from contracts.pose import Pose
from contracts.state import State
from goToGoal import GoToGoal
from robot import Robot

class Supervisor:
    def __init__(self):
        self.v = 0.2
        self.theta_d = math.pi/2.0
        self.pose = Pose(0,0,math.pi/2.0)
        self.leftTicks = 0
        self.rightTicks = 0
        self.controller = GoToGoal()
        self.robot = Robot()
        self.goal_x = 0.0
        self.goal_y = 0.5
        self.stopDistance = 0.05

    def execute(self, x, y):
        """"Executes the current controller."""
        v, w = self.controller.execute(x, y)

        vel_r, vel_l = self._ensureW(v, w)

        #print("real_v", vel_r, vel_l)

        # set robot wheel speeds
        self.robot.setMotorsSpeed(vel_r, vel_l)
    
    def _ensureW(self, v, w):
        """This function ensures that w is respected as best as possible by sacrificing v."""

        R = self.robot.R
        L = self.robot.L

        vel_max = self.robot.max_vel
        vel_min = self.robot.min_vel

        if (abs(v) > 0):
            # 1. Limit v, w to be possible in the range [vel_min, vel_max]
            v_lim = max(min(abs(v), (R/2.0)*(2*vel_max)), (R/2.0)*(2*vel_min))
            w_lim = max(min(abs(w), (R/L)*(vel_max-vel_min)), 0)
            

            # 2. Compute desired vel_r, vel_l needed to ensure w
            vel_r_d, vel_l_d = self.robot.uniToDiff(v_lim, w_lim)

            # 3. Find the max and min vel_r/vel_l
            vel_rl_max = max(vel_r_d, vel_l_d)
            vel_rl_min = min(vel_r_d, vel_l_d)

            # 4. Shift vel_r and vel_l if they exceed max/min velocity
            if (vel_rl_max > vel_max):
                vel_r = vel_r_d - (vel_rl_max - vel_max)
                vel_l = vel_l_d - (vel_rl_max - vel_max)
            elif (vel_rl_min < vel_min):
                vel_r = vel_r_d + (vel_min - vel_rl_min)
                vel_l = vel_l_d + (vel_min - vel_rl_min)
            else:
                vel_r = vel_r_d
                vel_l = vel_l_d
            
            # 5. Fix signs
            v_shift, w_shift = self.robot.diffToUni(vel_r, vel_l)

            v = math.copysign(1, v)*v_shift
            w = math.copysign(1, w)*w_shift

        else:
            # Robot is stationary, so we can either not rotate, or 
            # rotate with some minimum/maximum angular velocity
            w_min = (R/L)*(2*vel_min)
            w_max = (R/L)*(2*vel_max)

            if (abs(w) > w_min):
                w = math.copysign(1, w)*max(min(abs(w), w_max), w_min)
            else:
                w = 0

        vel_r, vel_l = self.robot.uniToDiff(v, w)

        return (vel_r, vel_l)

    def _updateOdometry(self, dt):
        """
        Approximates the location of the robot. Function should be called
        from the execute function every iteration.The location of the robot is
        updated based on the difference to the previous wheel encoder ticks.
        """
    
        R = self.robot.R
        L = self.robot.L

         # get left ticks from encoder
        #leftTicks = self.robot.wheelEncoders.leftTicks
        # get right ticks from encoder
        #rightTicks = self.robot.wheelEncoders.rightTicks

        m_vel_l = self.robot.leftWheel.v
        m_vel_r = self.robot.rightWheel.v

        #print("velocities", m_vel_l, m_vel_r)

        leftTicks = self.leftTicks + math.ceil((12*dt*abs(m_vel_l))/(R*math.pi))
        rightTicks = self.rightTicks + math.ceil((12*dt*abs(m_vel_r))/(R*math.pi))
        
        #leftTicks = self.leftTicks + 15
        #rightTicks = self.rightTicks + 15

        #print("ticks", leftTicks, rightTicks)

        #TODO: check if robot wheel was moving in backwards, then we must
        #       subtract ticks counter

        prevLeftTicks = self.leftTicks
        prevRightTicks = self.rightTicks

        self.rightTicks = leftTicks
        self.leftTicks = rightTicks

        mPerTick = (2.0*math.pi*R)/self.robot.ticksPerRev

        # distance traveled by right wheel
        dRight = (rightTicks - prevRightTicks)*mPerTick
        # distance traveled by left wheel
        dLeft = (leftTicks - prevLeftTicks)*mPerTick

        if(self.robot.leftWheel.direction == Direction.backward()):
            dRight = (-1)*dRight

        if (self.robot.rightWheel.direction == Direction.backward()):
            dLeft = (-1)*dLeft


        dCenter = (dRight + dLeft)/2.0
        phi = (dRight - dLeft)/L

        x = self.pose.x
        y = self.pose.y
        theta = self.pose.theta

        # model
        x_dt = dCenter*math.cos(theta)
        y_dt = dCenter*math.sin(theta)
        theta_dt = phi

        thetaNew = theta + theta_dt
        xNew = x + x_dt
        yNew = y + y_dt
        
        theta_atan2 = math.atan2(math.sin(thetaNew), math.cos(thetaNew))
        self.pose = Pose(xNew, yNew, theta_atan2)
        #print(self.pose)
    
    def _atGoal(self, state):
        x, y, theta = state.pose
        x_goal = self.goal_x
        y_goal = self.goal_y

        if math.hypot(x - x_goal, y - y_goal) < self.stopDistance:
            print("STOP")
            self.robot.stop()
            raise KeyboardInterrupt
    
    def stop(self):
        self.robot.stop()


        
