var socketio = io();

socketio.on('connect', function() {
	console.log('DAV');
});

var R = 0.028;
var L = 0.0105;

function uniToDiff(v, w) {
	vr = (2*v + w*L)/(2*R);
	vl = (2*v - w*L)/(2*R);

	return {
		vr: vr,
		vl: vl
	};
}

function calculateW(dx, dy) {
	var r = distanceFromOrigin(dx, dy);
	var phi = closingAngle(dx, r);
	return (-2)*phi + Math.PI;
}

function calculateV(dx, dy) {
	var r = distanceFromOrigin(dx, dy);
	return r*Math.sign(dy);
}

function distanceFromOrigin(x, y) {
	return Math.sqrt(x*x + y*y);
}

function closingAngle(x, r) {
	return Math.acos(x/r);
}

function limitVelocity(v) {
	return 0.30/50*v;
}

function evaluateVelocities(dx, dy) {
	var v = calculateV(dx, dy);
	var w = calculateW(dx, dy);
	//return uniToDiff(v, w);
	return {
		v: v,
		w: w
	};
}
    
var joystick = new VirtualJoystick({
    container: document.getElementById('container'),
        mouseSupport    : true,
        limitStickTravel: true,
        stickRadius: 50
});

function delta(dx, dy) {
	if (Math.abs(prevDx - dx) <= 2 || Math.abs(prevDy - dy) <= 2) {
		return false;
	}
	return true;
}

var vel_l = 0;
var vel_r = 0;

var prev_vel_l = 0;
var prev_vel_r = 0;
 
setInterval(function(){
    dx = joystick.deltaX();
    dy = (-1)*joystick.deltaY();

    var v = calculateV(dx, dy);
    var w = calculateW(dx, dy);


    ///////////////////////////////////////////////////////////////////
    // This should be handled on the backend
    //////////////////////////////////////////////////////////////////
    
    // forward
    if (w >= -0.90 && w <= 0.90) {
    	if (Math.abs(v) <= 5) {
    		//console.log("vel_l", 0.0);
    		//console.log("vel_r", 0.0);
    		vel_l = 0.0;
    		vel_r = 0.0;
    	} else if (Math.abs(v) > 5 && Math.abs(v) <= 20) {
    		//console.log("vel_l", 0.10);
    		//console.log("vel_r", 0.10);
    		vel_l = Math.sign(v)*0.11;
    		vel_r = Math.sign(v)*0.1;
    	} else if (Math.abs(v) > 20 && Math.abs(v) <= 40) {
    		//console.log("vel_l", 0.20);
    		//console.log("vel_r", 0.20);
    		vel_l = Math.sign(v)*0.16;
    		vel_r = Math.sign(v)*0.14;
    	} else if (Math.abs(v) > 40 && Math.abs(v) <= 50.1) {
    		//console.log("vel_l", 0.28);
    		//console.log("vel_r", 0.28);
    		vel_l = Math.sign(v)*0.25;
    		vel_r = Math.sign(v)*0.22;
    	}
    // left 2. degree
    } else if (w >= -2.4 && w <= -0.9) {
    	if (Math.abs(v) <= 5) {
    		//console.log("vel_l", 0.0);
    		//console.log("vel_r", 0.0);
    		vel_l = 0.0;
    		vel_r = 0.0;
    	} else if (Math.abs(v) > 5 && Math.abs(v) <= 20) {
    		//console.log("vel_l", 0.04);
    		//console.log("vel_r", 0.10);
    		vel_l = Math.sign(v)*0.04;
    		vel_r = Math.sign(v)*0.10;
    	} else if (Math.abs(v) > 20 && Math.abs(v) <= 40) {
    		//console.log("vel_l", 0.10);
    		//console.log("vel_r", 0.20);
    		vel_l = Math.sign(v)*0.05;
    		vel_r = Math.sign(v)*0.15;
    	} else if (Math.abs(v) > 40 && Math.abs(v) <= 50.1) {
    		//console.log("vel_l", 0.20);
    		//console.log("vel_r", 0.28);
    		vel_l = Math.sign(v)*0.08;
    		vel_r = Math.sign(v)*0.25;
    	}
    // left 4. degree
    } else if (w >= -Math.PI && w <= -2.4) {
    	if (Math.abs(v) <= 5) {
    		//console.log("vel_l", 0.0);
    		//console.log("vel_r", 0.0);
    		vel_l = 0.0;
    		vel_r = 0.0;
    	} else if (Math.abs(v) > 5 && Math.abs(v) <= 20) {
    		//console.log("vel_l", 0.01);
    		//console.log("vel_r", 0.10);
    		vel_l = Math.sign(v)*0.01;
    		vel_r = Math.sign(v)*0.10;
    	} else if (Math.abs(v) > 20 && Math.abs(v) <= 40) {
    		//console.log("vel_l", 0.05);
    		//console.log("vel_r", 0.20);
    		vel_l = Math.sign(v)*0.05;
    		vel_r = Math.sign(v)*0.15;
    	} else if (Math.abs(v) > 40 && Math.abs(v) <= 50.1) {
    		//console.log("vel_l", 0.12);
    		//console.log("vel_r", 0.28);
    		vel_l = Math.sign(v)*0.05;
    		vel_r = Math.sign(v)*0.20;
    	}
    }
    // right 2. degree
     else if (w >= 0.9 && w <= 2.4) {
    	if (Math.abs(v) <= 5) {
    		//console.log("vel_l", 0.0);
    		//console.log("vel_r", 0.0);
    		vel_l = 0.0;
    		vel_r = 0.0;
    	} else if (Math.abs(v) > 5 && Math.abs(v) <= 20) {
    		//console.log("vel_l", 0.1);
    		//console.log("vel_r", 0.04);
    		vel_l = Math.sign(v)*0.10;
    		vel_r = Math.sign(v)*0.01;
    	} else if (Math.abs(v) > 20 && Math.abs(v) <= 40) {
    		//console.log("vel_l", 0.20);
    		//console.log("vel_r", 0.10);
    		vel_l = Math.sign(v)*0.15;
    		vel_r = Math.sign(v)*0.05;
    	} else if (Math.abs(v) > 40 && Math.abs(v) <= 50.1) {
    		//console.log("vel_l", 0.28);
    		//console.log("vel_r", 0.20);
    		vel_l = Math.sign(v)*0.25;
    		vel_r = Math.sign(v)*0.08;
    	}
    // right 4. degree
    } else if (w >= 2.4 && w <= Math.PI) {
    	if (Math.abs(v) <= 5) {
    		//console.log("vel_l", 0.0);
    		//console.log("vel_r", 0.0);
    		vel_l = 0.0;
    		vel_r = 0.0;
    	} else if (Math.abs(v) > 5 && Math.abs(v) <= 20) {
    		//console.log("vel_l", 0.10);
    		//console.log("vel_r", 0.01);
    		vel_l = Math.sign(v)*0.10;
    		vel_r =Math.sign(v)* 0.01;
    	} else if (Math.abs(v) > 20 && Math.abs(v) <= 40) {
    		//console.log("vel_l", 0.20);
    		//console.log("vel_r", 0.05);
    		vel_l = Math.sign(v)*0.15;
    		vel_r = Math.sign(v)*0.05;
    	} else if (Math.abs(v) > 40 && Math.abs(v) <= 50.1) {
    		//console.log("vel_l", 0.12);
    		//console.log("vel_r", 0.28);
    		vel_l = Math.sign(v)*0.20;
    		vel_r = Math.sign(v)*0.05;
    	}
    } else {
		vel_l = 0.0;
		vel_r = 0.0;
	}

    // send only if there is velocity changes
    if (prev_vel_l != vel_l || prev_vel_r != vel_r) {
    	prev_vel_l = vel_l;
    	prev_vel_r = vel_r;
    	//console.log("vel_l", vel_l);
    	//console.log("vel_r", vel_r);
    	socketio.emit('forward', {
    		vel_l: vel_l,
    		vel_r: vel_r
    	});
    }
}, 1/30 * 1000);