from contracts.direction import Direction
import RPi.GPIO as GPIO
import time
from setup import fw_right, bw_right, fw_left, bw_left
from threading import Thread
import math

class WheelController:
    def __init__(self, forwPin, backPin):
        self.forwardPin = forwPin
        self.backwardPin = backPin
        self._speeds = {0: 0, 0.07812: 0.02, 0.10747: 0.025, 0.13941: 0.028, 0.15936: 0.03, 0.19019:0.035, 0.20409: 0.04, 0.23871: 0.05, 0.26616: 0.06, 0.27618: 0.09, 0.28841: 0.08}
        self.wheelSpeed = 0
        self.actualSpeed = 0

        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(self.forwardPin, GPIO.OUT)
        GPIO.setup(self.backwardPin, GPIO.OUT)

        self.task = Thread(target=self._runThread)
        self.task.setDaemon(1)
        self.task.start()
        
        self.direction = Direction.forward()

    def _speedToFreq(self, speed):
        target = 0
        smallest = 100
        for robotSpeed in self._speeds.keys():
            diff = abs(robotSpeed - abs(speed))
            if diff < smallest:
                smallest = diff
                target = robotSpeed
                self.actualSpeed = robotSpeed
        
        return self._speeds[target]

    def _getDirection(self, speed):
        sign = math.copysign(1, speed)
        if sign == 1:
            return Direction.forward()
        else:
            return Direction.backward()

    def stop(self):
        GPIO.output(self.forwardPin, 0)
        GPIO.output(self.backwardPin, 0)
        
    def _setWheelSpeed(self):
        freq = self._speedToFreq(self.wheelSpeed)
        self.direction = self._getDirection(self.wheelSpeed)
        if(self.direction == Direction.forward()):
            GPIO.output(self.forwardPin, 1)
            time.sleep(freq)
            GPIO.output(self.forwardPin, 0)
            cycle = 0.1 - freq
            time.sleep(cycle)
        elif(self.direction == Direction.backward()):
            GPIO.output(self.backwardPin, 1)
            time.sleep(freq)
            GPIO.output(self.backwardPin, 0)
            cycle = 0.1 - freq
            time.sleep(cycle)
    
    def _runThread(self):
        while True:
            try:
                self._setWheelSpeed()
            except KeyboardInterrupt:
                self.stop()
                break

if __name__ == "__main__":
    rightController = WheelController(fw_right, bw_right)
    leftController = WheelController(fw_left, bw_left)

    while True:
        try:
            rightController.wheelSpeed = 0.2
        except KeyboardInterrupt:
            rightController.stop()
            break
