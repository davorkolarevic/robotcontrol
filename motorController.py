from contracts.direction import Direction
import math
import RPi.GPIO as GPIO
from setup import fw_right, bw_right, fw_left, bw_left
import time
from threading import Thread

class MotorController:
    def __init__(self, forwardPin, backwardPin):
        self.v = 0.0

        self._forwardPin = forwardPin
        self._backwardPin = backwardPin

        GPIO.setmode(GPIO.BCM)
        
        try:
            GPIO.setup(self._forwardPin, GPIO.OUT)
            GPIO.setup(self._backwardPin, GPIO.OUT)
        except:
            pass

        self._fw = GPIO.PWM(self._forwardPin, 500)
        self._bw = GPIO.PWM(self._backwardPin, 1000)
        self._fw.start(0)
        self._bw.start(0)

        self.task = Thread(target=self._runThread)
        self.task.setDaemon(1)
        self.task.start()
        
        self.direction = Direction.forward()
    
    def fw(self,v):
        pwm = self._velocityToPwm(v)
        self._stopBw()
        self._fw.ChangeDutyCycle(pwm)
    
    def bw(self,v):
        pwm = self._velocityToPwm(v)
        self._stopFw()
        self._bw.ChangeDutyCycle(pwm)

    def _velocityToPwm(self, v):
        """Convert from velocity to pwm values based on maximum robot velocity. 
            Maximum Robot velocity should match 100 duty cycle.
        """
        pwm = abs(v)*333.33333
        adjustedPWM = max(min(round(pwm), 100), -100)
        return adjustedPWM


    def _stopFw(self):
        self._fw.ChangeDutyCycle(0)

    def _stopBw(self):
        self._bw.ChangeDutyCycle(0)

    def stop(self):
        self.v = 0
        self._stopFw()
        self._stopBw()

    def shutDown(self):
        self._fw.stop()
        self._bw.stop()
    
    def _getDirection(self, v):
        sign = math.copysign(1, v)
        if sign == 1:
            return Direction.forward()
        else:
            return Direction.backward()
    
    def _runMotor(self):
        self.direction = self._getDirection(self.v)
        if self.direction == Direction.forward():
            self.fw(self.v)
        else:
            self.bw(self.v)

    def _runThread(self):
        while True:
            try:
                self._runMotor()
            except KeyboardInterrupt:
                break


if __name__ == '__main__':
    rightMotor = MotorController(fw_right, bw_right)
    leftMotor = MotorController(fw_left, bw_left)
    counter = 0
    while True:
        try:
            leftMotor.v = 0.2
        except KeyboardInterrupt:
            leftMotor.stop()
            break
