"""
     This is a setup module. Define all GPIO constants here
"""

# wheels GPIO
fw_left = 23  # runs left wheel forward
fw_right = 24 # runs right wheel forward
bw_left = 25  # runs left wheel backward
bw_right = 22 # runs right wheel backward
