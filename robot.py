"""
================================================================================
    Robot module is responsible for main movement functionalities of robot.
================================================================================
"""


import math
from contracts.pose import Pose
from setup import fw_right, bw_right, fw_left, bw_left
from contracts.encoderType import EncoderType
from contracts.wheelsInfo import WheelsInfo
from motorController2 import MotorController2
#from wheelEncoderV2 import WheelEncoders
from motorController import MotorController
import time

class Robot:
    """
        Initialize robot states.
        R - wheels radius in meters.
        L - base length, distance between wheels in meters.
        ticksPerRev - number of encoder ticks per one wheel revolution

    """
    def __init__(self):
        self.R = 0.0028
        self.L = 0.0105
        self.ticksPerRev = 24
        self.max_vel = 0.20
        self.min_vel = 0
    
        self.pose = Pose(0,0,0)

        self.rightWheel = MotorController2(fw_right, bw_right)
        self.leftWheel = MotorController2(fw_left, bw_left)
        
        #self.wheelEncoders = WheelEncoders()

    """
        Transforms differential drive model wheel velocity to unicycle
        wheel velocity.
        vl - left wheel speed
        vr - right wheel speed

        Returns tuple of uncycle model linear velocity and angular velocity
    """
    def diffToUni(self, vl, vr):
        v = (vl + vr) * self.R/2
        w = (vr - vl) * self.R/self.L

        return (v,w)

    """
        Transforms uncycle model velocity to differential drive wheel velocity.
        v - linear velocity
        w - angular velocity

        Returns tuple of differential model left wheel velocity and
        right wheel velocity.
    """
    def uniToDiff(self, v, w):
        vr = (2.0*v + w*self.L)/(2.0*self.R)
        vl = (2.0*v - w*self.L)/(2.0*self.R)

        return (vr, vl)

    """
        Updates robot position based on wheel encoder ticks.
    """
    def updateState(self):
        # get left ticks from encoder
        leftTicks = self.wheelEncoders.leftTicks
        # get right ticks from encoder
        rightTicks = self.wheelEncoders.rightTicks

        prevLeftTicks = self.leftTicks
        prevRightTicks = self.rightTicks

        self.rightTicks = leftTicks
        self.leftTicks = rightTicks

        mPerTick = float(2*math.pi*self.R)/self.ticksPerRev

        # distance traveled by right wheel
        dRight = float(rightTicks - prevRightTicks)*mPerTick
        # distance traveled by left wheel
        dLeft = float(leftTicks - prevLeftTicks)*mPerTick

        dCenter = float(dRight + dLeft)/2.0
        phi = float(dRight - dLeft)/self.L

        x = self.pose.x
        y = self.pose.y
        theta = self.pose.theta

        theta_new = theta + phi
        xNew = x + dCenter*math.cos(theta)
        yNew = y + dCenter*math.sin(theta)
        
        return Pose(xNew, yNew, thetaNew)

    def setMotorsSpeed(self, rightWheelSpeed, leftWheelSpeed):
        vel_r, vel_l = self._limitSpeeds(rightWheelSpeed, leftWheelSpeed)
        self.rightWheel._runMotor(vel_r)
        self.leftWheel._runMotor(vel_l)

    
    def _limitSpeeds(self, rightWheelSpeed, leftWheelSpeed):
        vel_r = max(min(rightWheelSpeed, self.max_vel), -self.max_vel)
        vel_l = max(min(leftWheelSpeed, self.max_vel), - self.max_vel)

        # TODO: finish functionalities
        if (abs(vel_r) < self.min_vel):
            vel_r = 0
        if (abs(vel_l) < self.min_vel):
            vel_l = 0

        return (vel_r, vel_l)
    
    def stop(self):
        self.leftWheel.stop()
        self.rightWheel.stop()

if __name__ == '__main__':
    robot = Robot()
    counter = 0
    while True:
        try:
            time.sleep(0.01)
            if (counter <= 100):
                counter += 1
                #print(counter)
                robot.setWheelsSpeed(0.25,0.25)
            elif(counter > 100):
                robot.setWheelsSpeed(-0.25, -0.25)
        except KeyboardInterrupt:
            robot.stop()
            break
    
    
