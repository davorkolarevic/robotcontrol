class Direction(object):
    
    @staticmethod
    def forward():
        return 1;

    @staticmethod
    def backward():
        return 0;
