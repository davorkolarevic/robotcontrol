import numpy as np

class Pose(object):
    def __init__(self, *args):
        if len(args) == 0:
            self.set_pose(0,0,0)
        elif len(args) == 1:
            self.set_pose(*args[0])
        elif len(args) == 2:
            self.set_pose(args[0], args[2])
        elif len(args) == 3:
            self.set_pose(*args)
        else:
            raise ValueError("Invalid way to initialize a pose")

    def set_pose(self, *args, **kwargs):
        if len(args) == 3:
            self.x = float(args[0])
            self.y = float(args[1])
            self.theta = float(args[2])
        elif len(args) == 0:
            if 'x' in kwargs:
                self.x = float(kwargs['x'])
            if 'y' in kwargs:
                self.y = float(kwargs['y'])
            if 'theta' in kwargs:
                self.theta = float(kwargs['theta'])
        elif len(args) == 1 and isinstance(args[0], Pose):
            self.x = args[0].x
            self.y = args[0].y
            self.theta = args[0].theta

    def get_list(self):
        return [self.x, self.y, self.theta]

    def __iter__(self):
        yield self.x
        yield self.y
        yield self.theta

    def get_transformation(self):
        T = np.array([\
            [np.cos(self.theta), -np.sin(self.theta), self.x],\
            [np.sin(self.theta), np.cos(self.theta), self.y],\
            [0, 0, 1.0]])
        return T

    def __str__(self):
        return "(%f, %f) %f" % (self.x, self.y, self.theta)

    
