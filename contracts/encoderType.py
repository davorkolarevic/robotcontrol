class EncoderType(object):
    @staticmethod
    def leftEncoder():
        return "left";

    @staticmethod
    def rightEncoder():
        return "right";
