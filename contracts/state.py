from goal import Goal
from pose import Pose

class State:
    def __init__(self):
        self.v = 0
        self.goal = Goal()
        self.pose = Pose()