"""
    Basic wheels information
    .
    R - wheels radius in meters.
    L - base length, distance between wheels in meters.
    ticksPerRev - number of encoder ticks per one wheel revolution

    leftTicks - number of left wheel ticks
    rightTicks - number of right wheel ticks
"""

class WheelsInfo(object):
    def __init__(self, R, L, ticksPerRev):
        self.R = R
        self.L = L
        self.ticksPerRev = ticksPerRev

        self.leftTicks = 0
        self.rightTicks = 0
