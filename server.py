from flask import Flask, request, send_from_directory
import os
import socketio
import eventlet
import sys
import json
from supervisor import Supervisor
from robot import Robot


io = socketio.Server()
app = Flask(__name__, static_url_path='')


robot = Robot()

@app.route("/", methods=['GET'])
def root():
    return send_from_directory('app', 'index.html')

@app.route("/<filename>", methods=['GET'])
def sendFiles(filename):
    return send_from_directory('app', filename)

@app.route("/lib/<filename>", methods=['GET'])
def sendLibFiles(filename):
    return send_from_directory('app/lib', filename)


@io.on('connect')
def connect(sid, environ):
    print('connect', sid)

@io.on('forward')
def forward(sid, obj):
    #supervisor.execute(obj['x'], obj['y'])
    #print(obj['x'], obj['y'])
    vel_l = obj['vel_l']
    vel_r = obj['vel_r']
    robot.setMotorsSpeed(vel_r, vel_l)

if __name__ == "__main__":
    try:
        app = socketio.Middleware(io, app)
        eventlet.wsgi.server(eventlet.listen(('', 3000)), app)
    except KeyboardInterrupt:
        sys.exit()
