import math
import numpy

class GoToGoal:
    def __init__(self):
        self.headingAngle = 0
        self.E = 0
        self.error_1 = 0
        self._kp = 4.0
        self._ki = 1.0
        self._kd = 1.0

    def restart(self):
        """set integral and differential errors to zero"""
        self.E = 0 # integral error
        self.error_1 = 0 # differential error

    def setParameters(self, params):
        self._kp = params.kp # proportional gain
        self._ki = params.ki # integral gain
        self._kd = params.kd # differential gain
    
    def execute(self, x, y):
        """Calculate errors and steer the robot"""

        # goal
        x_g, y_g = x, y

        # robot
        x_r = 0
        y_r = 0
        theta = 0
        #print("first", state.pose.theta)

        # distance between goal and robot in x and y direction
        u_x = x_g - x_r
        u_y = y_g - y_r

        # angle from robot to goal
        theta_g = math.atan2(u_y, u_x)

        # 2. Calculate the heading error

        # error between the goal angle and robot's angle
        temp = theta_g - theta
        e_k = math.atan2(math.sin(temp), math.cos(temp))

        # 3. Calculate PID for the steering angle

        # error for the proportional term
        e_P = e_k

        # error for the integral term. HINT: Approximate the integral using
        # the accumulated error E and the error for this time step e_k
        e_I = self.E + e_k*dt

        # error for the derivative term. HINT: Approximate the derrivative
        # using the previous error error_1 and the error for this time step e_k
        e_D = (e_k - self.error_1)/dt

        # PID 
        w = self._kp*e_P + self._ki*e_I + self._kd*e_D

        # save errors for next time step
        self.E = e_I
        self.error_1 = e_k

        # The linear velocity is given to us
        v = state.v

        return [v, w]

